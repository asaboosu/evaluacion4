import React from "react";
import { Container, Nav, Row } from "react-bootstrap";

export default class Navegador extends React.Component {
	
	render() {
    return (
      <Container>
        <Row>
          <Nav fill variant="tabs" defaultActiveKey="/home">
            <Nav.Item>
              <Nav.Link href="/" >Inicio</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="/gastos">Gastos</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="/ingresos">Ingresos</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link onClick={this.props.deslogear} >
                Deslogear
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Row>
      </Container>
    );
  }
}
