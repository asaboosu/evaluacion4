import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Formulario from "./Formulario/Formulario";

export default class Ingresos extends React.Component {
  //saludos desde las 4 am
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          Header: "#",
          accessor: "numero", // accessor is the "key" in the data
        },
        {
          Header: "Monto",
          accessor: "monto",
        },
        {
          Header: "Fecha",
          accessor: "fecha",
        },
      ],
      data: [
        {
          numero: "1",

          monto: "1000",

          fecha: "1-1-1111",
        },
      ],
    };
  }

  addRegistro = (registro) => {};

  delRegistro = (registro) => {};

  updRegistro = (registro) => {};

  render() {
    return (
      <Container>
        <Row>
          <Col>
            <Formulario
              columns={this.state.columns}
              data={this.state.data}
              titulo={"Ingresos"}
            ></Formulario>
          </Col>
        </Row>
      </Container>
    );
  }
}
