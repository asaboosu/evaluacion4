import React from "react";
import { Col, Container, Row } from "react-bootstrap";

export default class Inicio extends React.Component {
  
	constructor(props) {
    super(props);
    this.balance = 10000;
  }

  render() {
    return (
      <Container>
        <Row>
          <Col sm={6} className="mx-auto">
            <h2>Balance: ${this.balance}</h2>
          </Col>
        </Row>
      </Container>
    );
  }
}
