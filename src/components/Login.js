import React from "react";
import { Container, Row, Form, Button, Col, } from "react-bootstrap";

export default class Login extends React.Component {
  render() {
    return (
      <Container fluid>
        <Row >
          <Col className="mx-auto col-sm-6">
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Correo Alumno</Form.Label>
                <Form.Control type="email" placeholder="asd123@domi.nio" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Contrasenia</Form.Label>
                <Form.Control type="password" placeholder="************" />
              </Form.Group>
              <Button variant="primary" onClick={this.props.logear}>
                Logear
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}
