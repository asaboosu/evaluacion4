import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Ingreso from "./Ingreso";
import Tabla from "./Tabla";

export default class Formulario extends React.Component {
  
  render() {
    return (
      <Container>
        <hr></hr>
        <h2 className="text-center " >{this.props.titulo}</h2>
        <hr></hr>
        <Row>
          <Col>
            <Ingreso></Ingreso>
          </Col>
          <Col>
            <Tabla
              columns={this.props.columns}
              data={this.props.data}
            ></Tabla>
          </Col>
        </Row>
      </Container>
    );
  }
}
