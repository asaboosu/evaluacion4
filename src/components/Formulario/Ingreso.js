import React from "react";
import { Button, Form } from "react-bootstrap";

export default class Ingreso extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hora: false,
      monto: 0,
      fecha: "2017-1-1",
    };
  }

  componentDidMount() {
   this.getDate();
  }

  getDate() {
    let date = new Intl.DateTimeFormat("en-GB", { dateStyle: "short" }).format(
      new Date()
    );
    this.setState({ date });
  }

  onChange = (event) => {
    let nombreElemento = event.target.name + "";
    let value = event.target.value;
    this.setState({ nombreElemento : value });
  };

  render() {
    return (
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Nº</Form.Label>
          <Form.Control name="n" type="number" placeholder="#" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Monto</Form.Label>
          <Form.Control name="monto" type="number" placeholder="$1000" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Fecha</Form.Label>
          <Form.Control name="fecha" type="date" value="2000-01-01" />
        </Form.Group>
        <Button variant="primary" onClick={() => alert("accion ingresar")}>
          Submit
        </Button>
      </Form>
    );
  }
}
