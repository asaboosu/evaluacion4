import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import Login from "./components/Login";
import Navegador from "./components/Navegador";
import Inicio from "./components/Inicio";
import Ingresos from "./components/Ingresos";
import Gastos from "./components/Gastos";
import loginApi from "./data/loginApi";

//i like el spanglish


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logeado: false,
    };
  }

  getLogin = () => {
    let usuario = "correo12@inacapmail.cl";
    let contrasenia = "Avanzada2021*";
    loginApi.getLogin(usuario, contrasenia);
    this.setState({ logeado: true });
  };

  delLogin = () => {
    loginApi.delLogin();
    this.setState({ logeado: false });
  }

  render() {
    return (
      <Container fluid className="bg-light">
        <Router>
          <Switch>
            {this.state.logeado === true && (
              <>
                <Navegador deslogear={this.delLogin}></Navegador>
                <Route exact path="/">
                  <Inicio></Inicio>
                </Route>
                <Route exact path="/ingresos">
                  <Ingresos></Ingresos>
                </Route>
                <Route exact path="/gastos">
                  <Gastos></Gastos>
                </Route>
              </>
            )}
            {this.state.logeado === false && (
              <Login
                logear={this.getLogin}
              ></Login>
            )}
          </Switch>
        </Router>
      </Container>
    );
  }
}
