class loginApi {
  url = "https://www.codigo-alfa.cl/api/setLogin?";

  getLogin(usuario, contrasenia) {
   
    fetch('https://www.codigo-alfa.cl/api/setLogin?' + new URLSearchParams({
    user: usuario,
    pass: contrasenia,
    })).then(function(response) {
      return response.json();
      
    }).then(function(json){
      alert(JSON.stringify(json));
    })

    window.localStorage.setItem("logeado", true);
    return window.localStorage.getItem("logeado");
  }

  delLogin() {
    window.localStorage.setItem("logeado", false);
  }
}
const api = new loginApi();
export default api;
